#include "stack.h"
#include <stdlib.h>
#include <stdio.h>


extern int stackInit(IntStack *self) {
  IntStack s = {.value = -1, .next = NULL};
  *self = s;
  //printf("%d e \n",self->value);
  return 0;
}
extern void stackRelease(IntStack *self){
  if(stackIsEmpty(self)) {
    free(self);
    return;
  }
  if(!stackIsEmpty(self->next)) stackRelease(self->next);
  free(self);
}


extern void stackPush(IntStack *self, int i){
    IntStack *s = malloc(sizeof(IntStack));
    s->value = self->value;
    s->next = self->next;
    IntStack a = {.value = i, .next = s};
    *self = a;
}

extern int stackTop(const IntStack *self) {
  return self->value;
}

extern int stackPop(IntStack *self) {
  if(stackIsEmpty(self)) {
    fprintf(stderr,"Kein pop moeglich, da Stack leer ist.\n");
    return 0;
  }
  int x = self->value;
  *self = *(self->next);

  return x;
}

extern int stackIsEmpty(const IntStack *self) {
  if(self->value!=-1) return 0;
  else return 1;
}

extern void stackPrint(const IntStack *self) {
  fprintf(stderr,"Stack: {");
  while (!stackIsEmpty(self)) {
    
    printf( " %d ->",self->value);
    
    self = self->next;
    
  }
  printf(" NULL }\n");

}
